package inventory.service;

import inventory.model.*;
import inventory.persistance.PartRepository;
import inventory.persistance.ProductRepository;
import javafx.collections.ObservableList;

public class InventoryService {

    private PartRepository partRepository;
    private ProductRepository productRepository;

    public InventoryService(PartRepository partRepository, ProductRepository productRepository){
        this.partRepository = partRepository;
        this.productRepository =productRepository;
    }


    public void addInhousePart(String name, double price, int inStock, int min, int  max, int partDynamicValue){
        InhousePart inhousePart = new InhousePart(partRepository.getAutoPartId(), name, price, inStock, min, max, partDynamicValue);
        partRepository.addPart(inhousePart);
    }

    public void addOutsourcePart(String name, double price, int inStock, int min, int  max, String partDynamicValue){
        OutsourcedPart outsourcedPart = new OutsourcedPart(partRepository.getAutoPartId(), name, price, inStock, min, max, partDynamicValue);
        partRepository.addPart(outsourcedPart);
    }

    public void addProduct(String name, double price, int inStock, int min, int  max, ObservableList<Part> addParts){
        Product product = new Product(productRepository.getAutoProductId(), name, price, inStock, min, max, addParts);
        productRepository.addProduct(product);
    }

    public ObservableList<Part> getAllParts() {
        return partRepository.getAllParts();
    }

    public ObservableList<Product> getAllProducts() {
        return productRepository.getAllProducts();
    }

    public Part lookupPart(String search) {
        return partRepository.lookupPart(search);
    }

    public Product lookupProduct(String search) {
        return productRepository.lookupProduct(search);
    }

    public void updateInhousePart(int partIndex, InhousePart inhousePart){
        partRepository.updatePart(partIndex, inhousePart);
    }

    public void updateOutsourcedPart(int partIndex, OutsourcedPart outsourcedPart){
        partRepository.updatePart(partIndex, outsourcedPart);
    }

    public void updateProduct(int productIndex, Product product){
        productRepository.updateProduct(productIndex, product);
    }

    public void deletePart(Part part){ partRepository.deletePart(part); }

    public void deleteProduct(Product product){
        productRepository.deleteProduct(product);
    }

    public boolean isPartInUse(Part part){
        return !productRepository.getAllProducts().filtered(x -> x.lookupAssociatedPart(part.getName())!=null).isEmpty();
    }
}
