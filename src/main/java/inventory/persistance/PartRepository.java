package inventory.persistance;

import inventory.model.InhousePart;
import inventory.model.OutsourcedPart;
import inventory.model.Part;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.StringTokenizer;

public class PartRepository {
    Logger logger = Logger.getLogger(PartRepository.class);


    private String partsFilename = "data/parts.txt";
    private Inventory inventory;

    public PartRepository(Inventory inventory) {
        this.inventory = inventory;
        readParts();
    }

    public PartRepository(String partsFilename, Inventory inventory) {
        this.partsFilename = partsFilename;
        this.inventory = inventory;
        readParts();
    }

    public void readParts(){
        ClassLoader classLoader = ProductRepository.class.getClassLoader();
        File file = new File(classLoader.getResource(partsFilename).getFile());
        ObservableList<Part> listP = FXCollections.observableArrayList();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line = null;
            while((line=br.readLine())!=null){
                Part part=getPartFromString(line);
                if (part!=null)
                    listP.add(part);
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        inventory.setAllParts(listP);
    }

    private Part getPartFromString(String line){
        Part item=null;
        if (line==null|| line.equals("")) return null;
        StringTokenizer st=new StringTokenizer(line, ",");
        String type=st.nextToken();
        if (type.equals("I")) {
            int id= Integer.parseInt(st.nextToken());
            inventory.setAutoPartId(id);
            String name= st.nextToken();
            double price = Double.parseDouble(st.nextToken());
            int inStock = Integer.parseInt(st.nextToken());
            int minStock = Integer.parseInt(st.nextToken());
            int maxStock = Integer.parseInt(st.nextToken());
            int idMachine= Integer.parseInt(st.nextToken());
            item = new InhousePart(id, name, price, inStock, minStock, maxStock, idMachine);
        }
        if (type.equals("O")) {
            int id= Integer.parseInt(st.nextToken());
            inventory.setAutoPartId(id);
            String name= st.nextToken();
            double price = Double.parseDouble(st.nextToken());
            int inStock = Integer.parseInt(st.nextToken());
            int minStock = Integer.parseInt(st.nextToken());
            int maxStock = Integer.parseInt(st.nextToken());
            String company=st.nextToken();
            item = new OutsourcedPart(id, name, price, inStock, minStock, maxStock, company);
        }
        return item;
    }

    public void writeAllParts() {
        ClassLoader classLoader = ProductRepository.class.getClassLoader();
        File partsFile = new File(classLoader.getResource(partsFilename).getFile());

        ObservableList<Part> parts=inventory.getAllParts();

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(partsFile))) {
            for (Part p:parts) {
                logger.error(p.toString());
                bw.write(p.toString());
                bw.newLine();
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public void addPart(Part part){
        inventory.addPart(part);
        writeAllParts();
    }

    public int getAutoPartId(){
        return inventory.getAutoPartId();
    }

    public ObservableList<Part> getAllParts(){
        return inventory.getAllParts();
    }

    public Part lookupPart (String search){
        return inventory.lookupPart(search);
    }

    public void updatePart(int partIndex, Part part){
        inventory.updatePart(partIndex, part);
        writeAllParts();
    }

    public void deletePart(Part part){
        inventory.deletePart(part);
        writeAllParts();
    }
}
