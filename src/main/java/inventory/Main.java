package inventory;

import inventory.persistance.Inventory;
import inventory.persistance.PartRepository;
import inventory.persistance.ProductRepository;
import inventory.service.InventoryService;
import inventory.controller.MainScreenController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.log4j.Logger;


public class Main extends Application {

    static Logger logger = Logger.getLogger(Main.class);

    @Override
    public void start(Stage stage) throws Exception {
        Inventory inventory = new Inventory();
        PartRepository partRepository = new PartRepository(inventory);
        ProductRepository productRepository= new ProductRepository(inventory);
        InventoryService service = new InventoryService(partRepository, productRepository);
        logger.info(service.getAllProducts());
        logger.info(service.getAllParts());
        FXMLLoader loader= new FXMLLoader(getClass().getResource("/fxml/MainScreen.fxml"));

        Parent root=loader.load();
        MainScreenController ctrl=loader.getController();
        ctrl.setService(service);

        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
