package inventory.persistance;

import inventory.model.InhousePart;
import inventory.model.OutsourcedPart;
import inventory.model.Part;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class PartRepositoryUnitTest {

    private PartRepository repository;
    private Inventory inventory;

    private Part part1;
    private Part part2;
    private Part part3;
    private Part updatePart;
    private ObservableList<Part> partList;
    private ObservableList<Part> secondPartList;

    @BeforeEach
    public void setUp(){
        inventory = mock(Inventory.class);

        part1 = new InhousePart(1, "roata", 3, 10, 1, 20, 3022);
        part2 = new InhousePart(2, "volan", 3, 10, 1, 20, 3021);
        part3 = new OutsourcedPart(3, "schimbator", 3, 10, 1, 20, "VvssSRL");
        updatePart = new InhousePart(4, "ambreaj",3,10,1,20,3022);

        partList = FXCollections.observableArrayList();
        partList.addAll(part1, part2, part3);

        secondPartList = FXCollections.observableArrayList();

        repository = new PartRepository(inventory);

        setUpMockInventory();
    }

    private void setUpMockInventory(){
        Mockito.when(inventory.getAllParts()).thenReturn(partList);

        Mockito.doThrow(NullPointerException.class).when(inventory).addPart(null);

        Mockito.doAnswer((Answer<Void>) invocation -> {
            Object[] arguments = invocation.getArguments();
            if (arguments != null && arguments.length == 1 && arguments[0] != null) {
                Part part = (Part) arguments[0];
                partList.add(part);
            }
            return null;
        }).when(inventory).addPart(part1);

        Mockito.doAnswer((Answer<Void>) invocation -> {
            Object[] arguments = invocation.getArguments();
            if (arguments != null && arguments.length == 1 && arguments[0] != null) {
                Part part = (Part) arguments[0];
                partList.remove(part);
            }
            return null;
        }).when(inventory).deletePart(part1);

        Mockito.doAnswer((Answer<Void>) invocation -> {
            Object[] arguments = invocation.getArguments();
            if (arguments != null && arguments.length == 2 && arguments[0] != null && arguments[1] != null) {
                Integer index = (Integer) arguments[0];
                Part part = (Part) arguments[1];
                partList.set(index,part);
            }
            return null;
        }).when(inventory).updatePart(0,updatePart);
    }

    @Test
    void testGetAll() {
        assertEquals(3, repository.getAllParts().size());
    }

    @Test
    void testAddNull_ShouldThrowNpe() {
        assertThrows(NullPointerException.class, () -> repository.addPart(null));
    }

    @Test
    void testSuccessfullyDeletePart() {
        repository.deletePart(part1);
        assertEquals(2, repository.getAllParts().size());
    }

    @Test
    void testSuccessfullyAddPart() {
        repository.addPart(part1);
        assertEquals(4, repository.getAllParts().size());
    }

    @Test
    void testSuccessfullyUpdatePart(){
        repository.updatePart(0,updatePart);
        assertEquals(updatePart,repository.getAllParts().get(0));
    }
}
