package inventory.persistance;

import inventory.model.InhousePart;
import inventory.model.Part;
import inventory.model.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class InventoryUnitTest {

    private Inventory inventory;

    @BeforeEach
    void initialize() {
        inventory = new Inventory();
        ObservableList<Part> partList = FXCollections.observableArrayList();
        partList.add(new InhousePart(1, "volan", 3, 10, 1, 20, 3021));
        inventory.addProduct(new Product(1, "papusa", 5, 7, 2, 10, partList));
        inventory.addProduct(new Product(2, "masina", 5, 7, 2, 10, partList));
    }

    @Test
    void testLookupByName() {
        assertNotNull(inventory.lookupProduct("papusa"));
    }

    @Test
    void testLookupById() {
        assertNotNull(inventory.lookupProduct("1"));
    }

    @Test
    void testNotMatchingSearchQuery() {
        assertNull(inventory.lookupProduct("444"));
    }

    @Test
    void testLookupCar() {
        assertNotNull(inventory.lookupProduct("masina"));
    }

    @Test
    void testEmptyInventory() {
        inventory = new Inventory();
        assertNull(inventory.lookupProduct("1"));
    }
}
