package inventory.persistance;

import inventory.model.InhousePart;
import inventory.model.OutsourcedPart;
import inventory.model.Part;
import inventory.model.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(OrderAnnotation.class)
class ProductRepositoryUnitTest {
    private static final String fileName = "test/products.txt";
    private ProductRepository productRepository;
    private Product product1;
    private Product product2;
    private Product product3;
    private Product product4;
    private Product productWithNullParts;
    private Product productWithEmptyParts;
    private Product productWithOnePart;

    @BeforeAll
    void beforeAll() {
        Part part1 = new InhousePart(1, "volan", 3, 10, 1, 20, 3021);
        Part part2 = new InhousePart(2, "roata", 3, 10, 1, 20, 3022);
        Part part3 = new OutsourcedPart(3, "schimbator", 3, 10, 1, 20, "VvssSRL");
        ObservableList<Part> filledPartList = FXCollections.observableArrayList();
        filledPartList.addAll(part1, part2, part3);
        ObservableList<Part> emptyPartList = FXCollections.observableArrayList();
        ObservableList<Part> onePartList = FXCollections.observableArrayList();
        onePartList.add(part1);
        product1 = new Product(1, "masina", 12, 20, 5, 50, filledPartList);
        product2 = new Product(2, "camion", 12, 20, 5, 50, filledPartList);
        product3 = new Product(3, "tractor", 12, 20, 5, 50, filledPartList);
        product4 = new Product(4, "autobuz", 12, 20, 5, 50, filledPartList);
        productWithNullParts = new Product(5, "bicicleta", 12, 20, 5, 50, null);
        productWithEmptyParts = new Product(6, "totineta", 10, 1, 1, 2, emptyPartList);
        productWithOnePart = new Product(7, "toacla", 10, 1, 1, 2, onePartList);
    }

    @BeforeEach
    void setUp() {
        clearFile();
        this.productRepository = new ProductRepository(new Inventory(), fileName);
        productRepository.addProduct(product1);
        productRepository.addProduct(product2);
        productRepository.addProduct(product3);
        productRepository.addProduct(product4);
    }

    @Test
    @Order(1)
    @Tag("ECP")
    @DisplayName("valid index and valid part list")
    void updateProductOnValidIndexAndWithValidParts() {
        productRepository.updateProduct(1, product2);
        assertEquals(product2, productRepository.lookupProduct(product2.getName()));
        assertEquals(product2, productRepository.getAllProducts().get(1));
    }

    @Test
    @Order(2)
    @Tag("ECP")
    @DisplayName("exception on index exceeding length")
    void updateProductOnExceedingIndex() {
        assertThrows(IndexOutOfBoundsException.class,
                () -> productRepository.updateProduct(5, product2));
    }


    @Test
    @Order(3)
    @Tag("ECP")
    @DisplayName("exception on negative index")
    void updateProductOnNegativeIndex() {
        assertThrows(IndexOutOfBoundsException.class,
                () -> productRepository.updateProduct(-2, product2));
    }

    @Test
    @Order(4)
    @Tag("ECP")
    @DisplayName("exception on empty parts")
    void updateProductEmptyParts() {
        assertThrows(IllegalStateException.class,
                () -> productRepository.updateProduct(0, productWithEmptyParts));
    }

    @Test
    @Order(5)
    @Tag("ECP")
    @DisplayName("exception on null parts")
    void updateProductWithNullParts() {
        assertThrows(NullPointerException.class,
                () -> productRepository.updateProduct(0, productWithNullParts));
    }

    @Test
    @Order(6)
    @Tag("BVA")
    @DisplayName("checking index valid limits")
    void updateProductOnValidLimitIndexes() {
        productRepository.updateProduct(0, product2);
        assertEquals(product2, productRepository.getAllProducts().get(0));
        productRepository.updateProduct(3, product2);
        assertEquals(product2, productRepository.getAllProducts().get(3));
    }

    @Test
    @Order(7)
    @Tag("BVA")
    @DisplayName("checking index invalid limits")
    void updateProductOnInvalidLimitIndexes() {
        assertThrows(IndexOutOfBoundsException.class,
                () -> productRepository.updateProduct(-1, product2));
        assertThrows(IndexOutOfBoundsException.class,
                () -> productRepository.updateProduct(4, product2));
    }

    @Test
    @Order(8)
    @Tag("BVA")
    @DisplayName("checking part list size = 1")
    void updateProductWithOnePart() {
        productRepository.updateProduct(2, productWithOnePart);
        assertEquals(productWithOnePart, productRepository.getAllProducts().get(2));
    }

    @Test
    @Order(9)
    @Tag("BVA")
    @DisplayName("checking part list size = 0")
    void updateProductWithInvalidPartListSize() {
        assertThrows(IllegalStateException.class,
                () -> productRepository.updateProduct(0, productWithEmptyParts));
    }

    private void clearFile() {
        ClassLoader classLoader = ProductRepository.class.getClassLoader();
        try {
            File productsFile = new File(classLoader.getResource(fileName).getFile());
            new FileWriter(productsFile).close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}