package inventory.service;

import inventory.model.InhousePart;
import inventory.model.OutsourcedPart;
import inventory.model.Part;
import inventory.persistance.PartRepository;
import inventory.persistance.ProductRepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

public class ServiceUnitTest {
    private PartRepository partRepository;
    private ProductRepository productRepository;
    private InventoryService service;

    private Part part1;
    private Part part2;
    private Part part3;
    private ObservableList<Part> partList;

    @BeforeEach
    public void setUp() {
        partRepository = mock(PartRepository.class);
        productRepository = mock(ProductRepository.class);
        service = new InventoryService(partRepository, productRepository);

        part1 = new InhousePart(1, "roata", 3, 10, 1, 20, 3022);
        part2 = new InhousePart(2, "volan", 3, 10, 1, 20, 3021);
        part3 = new OutsourcedPart(3, "schimbator", 3, 10, 1, 20, "VvssSRL");

        partList = FXCollections.observableArrayList();
        partList.addAll(part1, part2, part3);

        setUpMockRepo();
    }

    private void setUpMockRepo() {
        Mockito.when(partRepository.getAllParts()).thenReturn(partList);
        Mockito.doThrow(NullPointerException.class).when(partRepository).deletePart(null);
    }

    @Test
    void testGetAllParts() {
        assertEquals(3, service.getAllParts().size());
    }

    @Test
    void testRemoveNull_ShouldThrowNpe() {
        assertThrows(NullPointerException.class, () -> service.deletePart(null));
    }
}
