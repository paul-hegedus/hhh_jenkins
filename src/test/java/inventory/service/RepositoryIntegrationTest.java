package inventory.service;

import inventory.model.InhousePart;
import inventory.model.Part;
import inventory.persistance.Inventory;
import inventory.persistance.PartRepository;
import inventory.persistance.ProductRepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyChar;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class RepositoryIntegrationTest {

    Inventory inventory;
    PartRepository partRepository;
    ProductRepository productRepository;
    InventoryService service;

    private ObservableList<Part> partList;
    private Part part1;

    @BeforeEach
    void setUp() {
        inventory  = mock(Inventory.class);
        productRepository = mock(ProductRepository.class);

        partRepository = new PartRepository(inventory);
        service = new InventoryService(partRepository, productRepository);

        part1 = new InhousePart(0,"AAA",5,5,1,10,2);
        partList = FXCollections.observableArrayList();
        partList.add(part1);

        Mockito.when(inventory.getAllParts()).thenReturn(partList);
        Mockito.doNothing().when(inventory).addPart(part1);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void addInhousePart() {
        service.addInhousePart("AAA",5,5,1,10,2);
        verify(inventory,times(1)).addPart(any(Part.class));
        //System.out.println(service.getAllParts());
    }

    @Test
    void getAllParts(){
        assertEquals(service.getAllParts().get(0),part1);
    }
}